<?php



  get_header();



  while ( have_posts() ) : the_post();



  $thumbnail = getPostImage(get_the_ID(), 'full');

  $category = getPostDisplayCategory(get_the_ID());

  $author = get_the_author();

  $date = get_the_time('jS F Y');

 	$mainPostId = get_the_ID();



 ?>

<?php if ( ! has_tag( 'infinitescroll' ) ) : ?>

	<!--<div id="RTK_ZoXg" style="overflow: hidden;"></div>-->

<?php endif; ?>

<main class="site-content">



  <section class="hero-post align--center">



          <div class="post post--large">

            <div class="article-hero post__image z--0" style="background-image:url(<?php echo $thumbnail ?>)"></div>

            <div class="post__info">

              <div class="pre-title pre-title--tag font--white font--12px"><?php echo $category ?></div>

              <h1 class="article-title news-title font--40px"><?php echo get_the_title(); ?></h1>

              <div class="divider"></div>

              <p class="post-data font--16px">By <?php echo $author ?></p>

              <p class="post-data font--16px"><?php echo $date ?></p>

              <div class="divider"></div>

              <div class="share-buttons">

                <p class="share">Share this article</p>



                <?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { ADDTOANY_SHARE_SAVE_KIT(); } ?>



              </div>

            </div>

          </div>



        </section>



        <section class="article-content align--center">

	  <?php if ( ! wp_is_mobile() && has_tag( 'infinitescroll' ) ) : ?>

                <div class="sticky-side-ad is-hidden">

                        <div id="RTK_Pu0O" style="overflow:hidden"></div>

                </div> <!--  .sticky-side-ad -->

          <?php endif; ?>

          <div class="container">







			<script>

			  (function (d,s,n,id) {

				var js, fjs = d.getElementsByTagName(s)[0];

				if (d.getElementById(id)) return;

				js = d.createElement(s);

				js.id = id;

				js.className = n;

				js.src = "https://mcd-sdk.playbuzz.com/embed/sdk.js?embedId=79232d50-9b4b-4156-9c66-b3601b0d98a2";

				fjs.parentNode.insertBefore(js, fjs);

			  }(document,'script','playbuzz-mcd-sdk','mcd-sdk-jssdk_dailyfeed_Player_VT'));

			</script>

			<pb-mcd embed-id="79232d50-9b4b-4156-9c66-b3601b0d98a2"></pb-mcd>



			<?php if ( ! has_tag( 'infinitescroll' ) ) : ?>

				<div id="<?php echo uniqid();?>"></div>

				<div id="RTK_DdY1" style="overflow:hidden"></div>

			<?php endif; ?>

			<p class="siteintro">Daily Feed is the home of social first news and entertainment. From celebrity gossip to nostalgia, our unique content is guaranteed to intrigue and inform. If you enjoy the following article please share with your family and friends.</p>

	

	    <?php

		if ( has_tag( 'infinitescroll' ) ) {

			if ( wp_is_mobile() ) {

				echo do_shortcode('[ajax_load_more nextpage="true" nextpage_post_id="'.get_the_ID().'" nextpage_scroll="250:500" post_type="post" scroll_distance="50" max_pages="0" button_label="Start Reading" button_loading_label="Loading More Content" cache="true" cache_id="9999'.get_the_ID().'" nextpage_urls="false"]');

			} else {

				echo do_shortcode('[ajax_load_more nextpage="true" nextpage_post_id="'.get_the_ID().'" nextpage_scroll="0:500" post_type="post" scroll_distance="50" max_pages="0" button_label="Start Reading" button_loading_label="Loading More Content" cache="true" cache_id="9999'.get_the_ID().'" nextpage_urls="false"]');

			}

		} else {

			the_content();

		}

	    ?>



            <?php

	      if ( ! has_tag( 'infinitescroll' ) ) :

                $nextPost = get_previous_post(true);

  		echo daily_feed_post_pages($nextPost);

	      endif;

            ?>



            <div class="share-buttons">

            <span class="share">Share this article</span>

            <?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { ADDTOANY_SHARE_SAVE_KIT(); } ?>

	    </div>

          </div>

        </section>



      <?php



      endwhile; // End of the loop.



      ?>

	<?php



        if(!$_GET['lazy']){



      ?>

      <section class="related-stories align--center">



        <div class="columns columns--4 container">



          <h2 class="page-title font--22px">Related Stories</h2>

          <div class="divider"></div>



          <?php

          $sticky = get_option( 'sticky_posts' );

          $relatedArgs = array(

            'posts_per_page'=>4,

            'orderby'=>'date',

            'post__not_in'=>array_merge(array($mainPostId), $sticky),

            'category_name'=>$category

          );



          $relatedQuery = new WP_Query($relatedArgs);



          while($relatedQuery->have_posts()) :



            $relatedQuery->the_post();



            $thumbnail = getPostImage(get_the_ID(), 'listing-thumb');

            $category = getPostDisplayCategory(get_the_ID());

            $author = get_the_author();

            $date = get_the_time('jS F Y');



            echo '<a class="post post--small col" href="' . get_permalink() . '">

                <div class="post__image-container"><div class="post__image z--0" style="background-image:url(' . $thumbnail . ')"></div>

                  <div class="pre-title pre-title--tag extra font--white font--12px">' . $category . '</div>

                </div>

                <div class="post__info">

                  <h1 class="news-title font--22px">' . get_the_title() . '</h1>

                  <div class="divider"></div>

                  <p class="post-data font--16px">By ' . $author . '</p>

                  <p class="post-data font--16px">' . $date . '</p>

                </div>

              </a>';



          endwhile;

	wp_reset_query();

          ?>



        </div>



      </section>

<?php



        }



      ?>





</main>

 <?php







 get_footer();



  ?>



